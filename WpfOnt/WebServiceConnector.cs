﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfOnt.OntoWeb;

namespace WpfOnt
{
    public static class WebServiceConnector
    {
        public static string ElIndex 
        {
            get
            {
                var webClient = OntologyWebSoapClient;
                var indexeItem = webClient.Config().Where(cfg => cfg.ConfigItem.ToLower() == "elindex").ToList().FirstOrDefault();
                return indexeItem.ConfigValueString;
            }
        }
        private static OntoWeb.OntoWeb ontoWebSoapClient;
        public static OntoWeb.OntoWeb OntologyWebSoapClient
        {
            get 
            {
                if (ontoWebSoapClient == null)
                {
                    ontoWebSoapClient = new OntoWeb.OntoWeb();

                }
                return ontoWebSoapClient;
            }
            
        }

    }
}
